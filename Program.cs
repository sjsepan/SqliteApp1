﻿using System;
using System.Data;
using System.Data.SQLite;
// using System.Data.SQLite.Generic;//included with using System.Data.SQLite
// using System.Data.SQLite.Linq; //separate package System.Data.SQLite.Linq, 
//  but throws a target warning; look for NuGet phg with 'Core' in the name
// using System.Data.SQLite.EF6; //separate package System.Data.SQLite.EF6? Probably also needs to be 'Core'.
//using Mono.Data.Sqlite; //the library you would use with Mono instead of dotnet


namespace SqliteApp1
{
    class Program
    {
        //NOTE: either string works for a file in the app root folder, 
        // but the latter seems to allow path information

        // static String cs = @"URI=file:test.sqlite";
        static String cs = @"Data Source=./test.sqlite";

        // connection-strings for in-memory DB

        // static String cs = @"Data Source=:memory:"; //in-memory database, lifetime is for duration of connection
        // static String cs = @"Data Source=InMemorySample;Mode=Memory;Cache=Shared"; //in-memory database, shared between connections
        // static String cs = @"URI=file::memory:";

        //temporary database: empty string?

        static void Main(String[] args)
        {
            try
            {
                Console.WriteLine("Hello, World!");

                ShowVersion();

                CreateTable();

                PreparedStatements();

                Data_Reader();

                ColumnHeaders();

                SelectWithDataTable();
            }
            catch (System.Exception ex)
            {
                Console.Error.WriteLine($"ex: {ex.Message}");
            }
        }

        private static void SelectWithDataTable()
        {
            SQLiteDataAdapter adapter = null;
            DataTable table = null;

            try
            {
                Console.WriteLine("SelectWithDataTable:");
                using (SQLiteConnection connection = new SQLiteConnection(cs))
                { 
                    String statement = "SELECT * FROM cars LIMIT 5";

                    table = new DataTable();
                    SQLiteCommand cmd;
                    connection.Open();  //Initiate connection to the db
                    cmd = connection.CreateCommand();
                    cmd.CommandText = statement;  
                    adapter = new SQLiteDataAdapter(cmd);
                    adapter.Fill(table); //fill the datasource

                    foreach (DataRow row in table.Rows)
                    {
                        //NOTE:INTEGER key is Int64, INT field is Int32
                        Console.WriteLine($"{row.Field<Int64>(0).ToString()} {row.Field<String>(1)} {row.Field<Int32>(2)}");
                    }

                    connection.Close();//optional
                }
            }
            catch (System.Exception ex)
            {
                Console.Error.WriteLine($"ex: {ex.Message}");
            }
        }


        private static void ColumnHeaders()
        {
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(cs))
                {
                    connection.Open();

                    String statement = "SELECT * FROM cars ORDER BY name";//LIMIT 5";

                    using (SQLiteCommand command = new SQLiteCommand(statement, connection))
                    {

                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            Console.WriteLine($"{reader.GetName(0),-3} {reader.GetName(1),-12} {reader.GetName(2),8}");

                            while (reader.Read())
                            {
                                Console.WriteLine($@"{reader.GetInt32(0),-3} {reader.GetString(1),-12} {reader.GetInt32(2),8}");
                            }

                            reader.Close();//optional
                        }
                    }

                    connection.Close();//optional
                }
            }
            catch (System.Exception ex)
            {
                Console.Error.WriteLine($"ex: {ex.Message}");
            }
        }

        private static void Data_Reader()
        {
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(cs))
                {
                    connection.Open();

                    String statement = "SELECT * FROM cars LIMIT 5";

                    using (SQLiteCommand command = new SQLiteCommand(statement, connection))
                    {
                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                Console.WriteLine($"{reader.GetInt32(0)} {reader.GetString(1)} {reader.GetInt32(2)}");
                            }

                            reader.Close();//optional

                        }
                    }

                    connection.Close();//optional
                }
            }
            catch (System.Exception ex)
            {
                Console.Error.WriteLine($"ex: {ex.Message}");
            }
        }

        private static void PreparedStatements()
        {
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(cs))
                {
                    connection.Open();

                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        command.CommandText = "INSERT INTO cars(name, price) VALUES(@name, @price)";

                        command.Parameters.AddWithValue("@name", "BMW");
                        command.Parameters.AddWithValue("@price", 36600);
                        command.Prepare();

                        command.ExecuteNonQuery();

                        Console.WriteLine("row inserted");
                    }

                    connection.Close();//optional
                }

            }
            catch (System.Exception ex)
            {
                Console.Error.WriteLine($"ex: {ex.Message}");
            }
        }

        private static void CreateTable()
        {
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(cs))
                {
                    connection.Open();

                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {

                        command.CommandText = "DROP TABLE IF EXISTS cars";
                        command.ExecuteNonQuery();

                        command.CommandText = @"CREATE TABLE cars(id INTEGER PRIMARY KEY, name TEXT, price INT)";
                        command.ExecuteNonQuery();

                        command.CommandText = "INSERT INTO cars(name, price) VALUES('Audi',52642)";
                        command.ExecuteNonQuery();

                        command.CommandText = "INSERT INTO cars(name, price) VALUES('Mercedes',57127)";
                        command.ExecuteNonQuery();

                        command.CommandText = "INSERT INTO cars(name, price) VALUES('Skoda',9000)";
                        command.ExecuteNonQuery();

                        command.CommandText = "INSERT INTO cars(name, price) VALUES('Volvo',29000)";
                        command.ExecuteNonQuery();

                        command.CommandText = "INSERT INTO cars(name, price) VALUES('Bentley',350000)";
                        command.ExecuteNonQuery();

                        command.CommandText = "INSERT INTO cars(name, price) VALUES('Citroen',21000)";
                        command.ExecuteNonQuery();

                        command.CommandText = "INSERT INTO cars(name, price) VALUES('Hummer',41400)";
                        command.ExecuteNonQuery();

                        command.CommandText = "INSERT INTO cars(name, price) VALUES('Volkswagen',21600)";
                        command.ExecuteNonQuery();

                        Console.WriteLine("Table cars created");
                    }

                    connection.Close();//optional
                }
            }
            catch (System.Exception ex)
            {
                Console.Error.WriteLine($"ex: {ex.Message}");
            }
        }

        private static void ShowVersion()
        {
            try
            {
                String cs = "Data Source=:memory:";
                String statement = "SELECT SQLITE_VERSION()";

                using (SQLiteConnection connection = new SQLiteConnection(cs))
                {
                    connection.Open();

                    using (SQLiteCommand command = new SQLiteCommand(statement, connection))
                    {
                        String version = command.ExecuteScalar().ToString();

                        Console.WriteLine($"SQLite version: {version}");
                    }

                    connection.Close();//optional
                }
            }
            catch (System.Exception ex)
            {
                Console.Error.WriteLine($"ex: {ex.Message}");
            }
        }
    }
}
