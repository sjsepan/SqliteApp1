# SqliteApp1

## About

C# dotnet (Core) Console app (sample) using System.Data.SQLite to illustrate simple db access. Based on sample at https://zetcode.com/csharp/sqlite/, and revised using other examples. Coded with classic C# coding styles.

## References

for this project:
https://zetcode.com/csharp/sqlite/

other references:
https://stackoverflow.com/questions/26020/what-is-the-best-way-to-connect-and-use-a-sqlite-database-from-c-sharp
https://www.codeguru.com/dotnet/using-sqlite-in-a-c-application/
https://jasonwatmore.com/post/2022/09/05/net-6-connect-to-sqlite-database-with-entity-framework-core
https://www.skotechlearn.com/2021/06/connect-SQLite-in-VB-Net.html

## Libraries

dotnet add package System.Data.SQLite.Core
dotnet add package System.Data.SQLite.Linq

## Defaults

-not using top-level statements
-not using file-scoped namespaces
-not using implicit usings
-not using 'var' keyword for declarations
-using older style 'new' statements with Type name
-using older style 'using' statements with braces

## Issues

-"/home/sjsepan/Projects/DotNetCoreProjects/SqliteApp1/SqliteApp1.csproj : warning NU1701: Package 'System.Data.SQLite.Linq 1.0.117' was restored using '.NETFramework,Version=v4.6.1, .NETFramework,Version=v4.6.2, .NETFramework,Version=v4.7, .NETFramework,Version=v4.7.1, .NETFramework,Version=v4.7.2, .NETFramework,Version=v4.8, .NETFramework,Version=v4.8.1' instead of the project target framework 'net7.0'. This package may not be fully compatible with your project."
Occurs when referencing this Linq NuGet package  in project: '<PackageReference Include="System.Data.SQLite.Linq" Version="1.0.117" />'; find 'Core' version

## Note

- In-memory + cached still seems to save a file, with the name from Data Source, in the app project root.
